package study.datajpa.entity;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ResponseStatus;
import study.datajpa.dto.MemberDto;
import study.datajpa.repository.MemberRepository;
import study.datajpa.repository.TeamRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional(readOnly = true)
@Rollback(value = false)
class MemberTest {
    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    EntityManager em;

    @Test
    @Transactional
    @Rollback(false)
    public void testEntity() {
        Team teamA = new Team("teamA");
        Team teamB = new Team("teamB");
        teamRepository.save(teamA);
        teamRepository.save(teamB);

        Member member1 = new Member("member1", 10, teamA);
        Member member2 = new Member("member2", 20, teamA);
        Member member3 = new Member("member3", 30, teamB);
        Member member4 = new Member("member4", 40, teamB);
        memberRepository.save(member1);
        memberRepository.save(member2);
        memberRepository.save(member3);
        memberRepository.save(member4);

        /*
        List<MemberDto> memberDto = memberRepository.findMemberDto();
        memberDto.stream().forEach(dto-> System.out.println(dto.getUsername()+" in team "+dto.getTeamName()));

        List<String> usernameList = memberRepository.findUsernameList();
        usernameList.stream().forEach(list -> System.out.println("list = " + list));
         */

        /*
        memberRepository.findByNames(Arrays.asList("member1", "member3"))
                .stream().forEach(list-> System.out.println("list.getUsername() = " + list.getUsername()));

         */

        /*
        int count = memberRepository.bulkAgePlus(20);
        System.out.println("count = " + count);
         */

        /*
        memberRepository.findMemberEntityGraph2().stream().forEach(m-> System.out.println("m.getTeam() = " + m.getTeam()));
         */

        List<Member> list = memberRepository.findMemberCustom();
        list.stream().forEach(m -> System.out.println("m.getUsername() = " + m.getUsername()));
    }

    @Test
    public void queryHint(){
        //given
        memberRepository.save(new Member("member1", 10));
        em.flush();
        em.clear();
        //when
        Member member = memberRepository.findReadOnlyByUsername("member1");
        member.setUsername("member2");
        em.flush(); //Update Query 실행X
    }

    @Test
    public void jpaLock(){
        //given
        memberRepository.save(new Member("member1", 10));
        em.flush();
        em.clear();
        //when
        Member member = memberRepository.findLockByUsername("member1");
        member.setUsername("member2");
        em.flush(); //Update Query 실행X
    }
}